package com.profile.profilemanagementservice.service;

import com.profile.profilemanagementservice.domain.TeacherProfile;
import com.profile.profilemanagementservice.exception.CustomNotFoundException;
import com.profile.profilemanagementservice.exception.GeneralBodyResponse;
import com.profile.profilemanagementservice.repository.TeacherProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class TeacherProfileService {

    
    private final TeacherProfileRepository teacherRepository;

    public TeacherProfileService(TeacherProfileRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public GeneralBodyResponse createTeacherProfile(TeacherProfile teacher) {
        if (teacher == null) {
            return new GeneralBodyResponse(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), "Teacher data cannot be null", null);
        }

        TeacherProfile createdTeacher = teacherRepository.save(teacher);
        return new GeneralBodyResponse(HttpStatus.CREATED.value(), HttpStatus.CREATED.getReasonPhrase(), "Successfully created Teacher Profile", createdTeacher);
    }

    public GeneralBodyResponse updateTeacherProfile(Long teacherId, TeacherProfile updatedTeacher) {
        TeacherProfile existingTeacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Teacher not found with ID: " + teacherId));

        existingTeacher.setFullName(updatedTeacher.getFullName());
        existingTeacher.setEmail(updatedTeacher.getEmail());
        existingTeacher.setDateOfBirth(updatedTeacher.getDateOfBirth());
        existingTeacher.setAddress(updatedTeacher.getAddress());
        existingTeacher.setPhoneNumber(updatedTeacher.getPhoneNumber());
        existingTeacher.setTeachingField(updatedTeacher.getTeachingField());
        existingTeacher.setTeacherId(updatedTeacher.getTeacherId());
        existingTeacher.setActive(updatedTeacher.isActive());

        teacherRepository.save(existingTeacher);
        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Successfully updated Teacher Profile", existingTeacher);
    }

    public GeneralBodyResponse deleteTeacherProfile(Long teacherId) {
        TeacherProfile existingTeacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Teacher not found with ID: " + teacherId));

        teacherRepository.delete(existingTeacher);
        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Successfully deleted Teacher Profile", teacherId);
    }

    public GeneralBodyResponse getTeacherProfile(Long teacherId) {
        TeacherProfile teacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Teacher not found with ID: " + teacherId));

        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Teacher Profile retrieved successfully", teacher);
    }
}
