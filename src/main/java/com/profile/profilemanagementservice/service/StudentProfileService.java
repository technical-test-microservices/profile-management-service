package com.profile.profilemanagementservice.service;

import com.profile.profilemanagementservice.domain.StudentProfile;
import com.profile.profilemanagementservice.exception.GeneralBodyResponse;
import com.profile.profilemanagementservice.repository.StudentProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentProfileService {

    @Autowired
    private StudentProfileRepository studentRepository;

    public GeneralBodyResponse createStudentProfile(StudentProfile student) {
        if (student == null) {
            return new GeneralBodyResponse(HttpStatus.BAD_REQUEST.value(), null, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Student data cannot be null");
        }

        StudentProfile createdStudent = studentRepository.save(student);
        return new GeneralBodyResponse(HttpStatus.CREATED.value(), HttpStatus.CREATED.getReasonPhrase(), "Successfully created Student Profile", createdStudent);
    }

    public GeneralBodyResponse updateStudentProfile(Long studentId, StudentProfile updatedStudent) {
        StudentProfile existingStudent = studentRepository.findById(studentId)
                .orElseThrow();

        existingStudent.setFullName(updatedStudent.getFullName());
        existingStudent.setEmail(updatedStudent.getEmail());
        existingStudent.setDateOfBirth(updatedStudent.getDateOfBirth());
        existingStudent.setAddress(updatedStudent.getAddress());
        existingStudent.setPhoneNumber(updatedStudent.getPhoneNumber());
        existingStudent.setGender(updatedStudent.getGender());
        existingStudent.setNisn(updatedStudent.getNisn());
        existingStudent.setActive(updatedStudent.isActive());

        studentRepository.save(existingStudent);
        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Successfully updated Student Profile", existingStudent);
    }

    public GeneralBodyResponse deleteStudentProfile(Long studentId) {
        StudentProfile existingStudent = studentRepository.findById(studentId)
                .orElseThrow();

        studentRepository.delete(existingStudent);
        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Successfully deleted Student Profile", studentId);
    }

    public GeneralBodyResponse getStudentProfile(Long studentId) {
        StudentProfile student = studentRepository.findById(studentId)
                .orElseThrow();

        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Student Profile retrieved successfully", student);
    }

    public GeneralBodyResponse getAllStudentProfiles() {
        List<StudentProfile> studentProfiles = studentRepository.findAll();
        if (studentProfiles.isEmpty()) {
            return new GeneralBodyResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase(), "No student profiles found.", null);
        }

        return new GeneralBodyResponse(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Student profiles retrieved successfully", studentProfiles);
    }
}
