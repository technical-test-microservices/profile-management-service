package com.profile.profilemanagementservice.controller;

import com.profile.profilemanagementservice.domain.StudentProfile;
import com.profile.profilemanagementservice.exception.GeneralBodyResponse;
import com.profile.profilemanagementservice.service.StudentProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/student-profiles")
public class StudentProfileController {

    @Autowired
    private StudentProfileService studentProfileService;

    @PostMapping
    public ResponseEntity<?> createStudentProfile(@RequestBody StudentProfile student) {
        GeneralBodyResponse response = studentProfileService.createStudentProfile(student);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @PutMapping("/{studentId}")
    public ResponseEntity<?> updateStudentProfile(@PathVariable Long studentId, @RequestBody StudentProfile updatedStudent) {
        GeneralBodyResponse response = studentProfileService.updateStudentProfile(studentId, updatedStudent);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @DeleteMapping("/{studentId}")
    public ResponseEntity<?> deleteStudentProfile(@PathVariable Long studentId) {
        GeneralBodyResponse response = studentProfileService.deleteStudentProfile(studentId);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{studentId}")
    public ResponseEntity<?> getStudentProfile(@PathVariable Long studentId) {
        GeneralBodyResponse response = studentProfileService.getStudentProfile(studentId);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping
    public ResponseEntity<?> getAllStudentProfiles() {
        GeneralBodyResponse response = studentProfileService.getAllStudentProfiles();
        return ResponseEntity
                .ok()
                .body(response);
    
    }
}
