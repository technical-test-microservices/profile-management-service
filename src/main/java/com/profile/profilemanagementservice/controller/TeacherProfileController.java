package com.profile.profilemanagementservice.controller;


import com.profile.profilemanagementservice.domain.TeacherProfile;
import com.profile.profilemanagementservice.exception.GeneralBodyResponse;
import com.profile.profilemanagementservice.service.TeacherProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/teachers")
public class TeacherProfileController {
    
    
    private final TeacherProfileService teacherService;

    public TeacherProfileController(TeacherProfileService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    public ResponseEntity<?> createTeacherProfile(@RequestBody TeacherProfile teacher) {
        GeneralBodyResponse response = teacherService.createTeacherProfile(teacher);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{teacherId}")
    public ResponseEntity<?> getTeacherProfile(@PathVariable Long teacherId) {
        GeneralBodyResponse response = teacherService.getTeacherProfile(teacherId);
        return ResponseEntity
                .ok()
                .body(response);

    }

    @PutMapping("/{teacherId}")
    public ResponseEntity<?> updateTeacherProfile(@PathVariable Long teacherId, @RequestBody TeacherProfile updatedTeacher) {
        GeneralBodyResponse response = teacherService.updateTeacherProfile(teacherId, updatedTeacher);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @DeleteMapping("/{teacherId}")
    public ResponseEntity<?> deleteTeacherProfile(@PathVariable Long teacherId) {
        GeneralBodyResponse response = teacherService.deleteTeacherProfile(teacherId);
        return ResponseEntity
                .ok()
                .body(response);
    }

}

