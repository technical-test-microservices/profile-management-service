package com.profile.profilemanagementservice.repository;

import com.profile.profilemanagementservice.domain.TeacherProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TeacherProfileRepository extends JpaRepository<TeacherProfile, Long> {
    
}
